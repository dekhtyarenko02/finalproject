package com.ua.dekhtiarenko.webapp.db.dao.interfaces;

import com.ua.dekhtiarenko.webapp.db.entity.User;

import java.util.List;

public interface UserDAOMethods {

    void insertUser(User user);

    User getUser(String email);

    List<User> findAllUsers();

    void updateUser(User user);

    void deleteUser(User user);
}
