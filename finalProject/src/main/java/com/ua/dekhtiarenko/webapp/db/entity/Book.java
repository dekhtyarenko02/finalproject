package com.ua.dekhtiarenko.webapp.db.entity;

public class Book {

    private int id;
    private String nameOfBook;
    private String author;
    private boolean availability;

    public Book(){

    }

    public Book(String nameOfBook, String author, boolean availability) {
        this.nameOfBook = nameOfBook;
        this.author = author;
        this.availability = availability;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfBook() {
        return nameOfBook;
    }

    public void setNameOfBook(String nameOfBook) {
        this.nameOfBook = nameOfBook;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", nameOfBook='" + nameOfBook + '\'' +
                ", author='" + author + '\'' +
                ", availability=" + availability +
                '}';
    }
}
